package com.ef.constants;

public class GlobalProperties {

    public static final String ACCESS_LOG_ARG_NAME = "accesslog";
    public static final String STARTDATE_ARG_NAME = "startDate";
    public static final String THRESHOLD_ARG_NAME = "threshold";
    public static final String DURATION_ARG_NAME = "duration";
    public static final String ARG_DATE_TIME_PATTERN = "yyyy-MM-dd.HH:mm:ss";

    public final static String delimiter = "\\|";

    public static final String accessLogTableName = "access_log";

    public static final String blockedHostsTableName = "blocked_hosts";

    //public static final String dbConnectionString = "jdbc:mysql://192.168.1.121/wh?user=root&password=pass" +
    //        "&useLegacyDatetimeCode=false&serverTimezone=America/Los_Angeles";

    //Database
    public static final String HOST_PORT_KEY = "host_port";
    public static final String SCHEMA_KEY = "schema";
    public static final String USERNAME_KEY = "user";
    public static final String PASSWORD_KEY = "pass";

    public final static String createAccessLogTableQuery = "create table if not exists " + accessLogTableName + " (" +
            "    id int not null AUTO_INCREMENT, " +
            "    event_time timestamp, " +
            "    ip  varchar(20), " +
            "    request varchar(100), " +
            "    http_status int, " +
            "    user_agent varchar(1000), " +
            "    PRIMARY KEY (id) " +
            " )";

    public final static String createBlokedHostsTableQuery = "create table if not exists " + blockedHostsTableName + "( " +
            "    id int not null AUTO_INCREMENT, " +
            "    ip  varchar(20),  " +
            "    request_count int, " +
            "    reason varchar(1000), " +
            "    PRIMARY KEY (id) " +
            ")";

    public final static String truncateAccessLogTableQuery = "truncate table " + accessLogTableName;

    public final static String truncateBlokedHostsTableQuery = "truncate table " + blockedHostsTableName;

    public final static String filterLogQuery = "select ip,count(1) as cnt from wh.access_log " +
            "where event_time between ? and ? group by ip having cnt > ? order by cnt desc";

    public final static String insertLogQuery = "insert into " + accessLogTableName + " (event_time, ip, request, http_status, user_agent)" +
            "    values ( ?, ?, ?, ?, ?)";

    public final static String insertBlockedHostsQuery = "insert into "+ blockedHostsTableName +" (ip, request_count, reason) " +
            "values (?, ?, ?)";

}
