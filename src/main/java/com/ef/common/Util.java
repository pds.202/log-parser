package com.ef.common;

import com.ef.constants.DurationEnum;
import com.ef.constants.GlobalProperties;
import com.ef.vo.CommandArgs;
import org.apache.commons.cli.*;

import javax.xml.bind.ValidationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Properties;

public class Util {

    public static LocalDateTime formatDateTime(String strDate, String dateTimePattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimePattern);
        return LocalDateTime.parse(strDate, formatter);
    }

    public static LocalDateTime getEndDateTime(LocalDateTime startDate, DurationEnum duration) {
        LocalDateTime result;
        if (duration == DurationEnum.daily) {
            result = startDate.plusDays(1);
        } else {
            result = startDate.plusHours(1);
        }
        return result;
    }

    public static String buildReason(LocalDateTime startDateTime, LocalDateTime endDateTime, int threshold, Integer numberOfCalls) {
        String reason = String.format("Made more than %s calls between %s and %s", threshold, startDateTime.toString(), endDateTime.toString());
        return reason;
    }

    public static Options parseCliArgs() {
        Options options = new Options();

        Option accessLog = new Option("a", GlobalProperties.ACCESS_LOG_ARG_NAME, true, "Path to log file to parse");
        Option startDate = new Option("s", GlobalProperties.STARTDATE_ARG_NAME, true, "Start Date with time yyyy-MM-dd.HH:mm:ss");
        Option threshold = new Option("t", GlobalProperties.THRESHOLD_ARG_NAME, true, "Numeric threshold value");
        Option duration = new Option("d", GlobalProperties.DURATION_ARG_NAME, true, "duration can only be hourly or daily. Default hourly");

        options.addOption(accessLog);
        options.addOption(startDate);
        options.addOption(duration);
        options.addOption(threshold);

        return options;
    }

    public static CommandArgs buildArgs(Options options, String[] args) {
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            String accessLogPath = cmd.getOptionValue(GlobalProperties.ACCESS_LOG_ARG_NAME);

            String startDateStr = cmd.getOptionValue(GlobalProperties.STARTDATE_ARG_NAME);
            LocalDateTime startDate = null;
            if (Objects.nonNull(startDateStr)) {
                startDate = Util.formatDateTime(startDateStr, GlobalProperties.ARG_DATE_TIME_PATTERN);
            }
            String thresholdStr = cmd.getOptionValue(GlobalProperties.THRESHOLD_ARG_NAME);
            Integer threshold = null;
            if (Objects.nonNull(thresholdStr)) {
                threshold = Integer.parseInt(thresholdStr);
            }

            DurationEnum duration = validateDuration(cmd.getOptionValue(GlobalProperties.DURATION_ARG_NAME, "hourly"));

            return new CommandArgs(accessLogPath, startDate, duration, threshold);

        } catch (Exception e) {
            System.err.println("[Error] buildArgs:: " + e.getMessage());
            formatter.printHelp("parser", options);
            System.exit(1);
            return null;
        }
    }

    public static DurationEnum validateDuration(String duration) throws ValidationException {
        if (duration.equals(DurationEnum.daily.toString()) || duration.equals(DurationEnum.hourly.toString())) {
            return DurationEnum.valueOf(duration);
        }
        throw new ValidationException("duration can only be hourly or daily");
    }


    private static String connectionString = null;

    public static String getConnectionString() throws Exception {

        String filename = "config.properties";
        String url;
        if (Objects.isNull(connectionString)) {
            Properties prop = new Properties();
            try {
                prop.load(new FileInputStream(filename));
            } catch (IOException e) {
                /*System.err.println("Error while reading properties. Please make sure there is " +filename+" file " +
                        "in same directory as parser.jar");*/

                System.out.println("[WARN]: cannot find " + filename + " file " + " in parser.jar's directory. Using default config");
                prop.load(Util.class.getClassLoader().getResourceAsStream(filename));
            }

            String hostAndPort = validateNotNull(GlobalProperties.HOST_PORT_KEY, prop.getProperty(GlobalProperties.HOST_PORT_KEY));
            String schema = validateNotNull(GlobalProperties.SCHEMA_KEY, prop.getProperty(GlobalProperties.SCHEMA_KEY));
            String user = validateNotNull(GlobalProperties.USERNAME_KEY, prop.getProperty(GlobalProperties.USERNAME_KEY));
            String pass = validateNotNull(GlobalProperties.PASSWORD_KEY, prop.getProperty(GlobalProperties.PASSWORD_KEY));

            url = "jdbc:mysql://" + hostAndPort + "/" + schema + "?user=" + user + "&password=" + pass +
                    "&useLegacyDatetimeCode=false&serverTimezone=America/Los_Angeles&useSSL=false";
        } else {
            url = connectionString;
        }
        return url;
    }

    private static String validateNotNull(String key, String value) throws Exception {
        if (Objects.nonNull(value)) {
            return value;
        } else {
            //System.err.println("Error while reading " + key + ". Please make sure the property is available in property file.");
            throw new Exception("Error while reading " + key + " from property file. Please make sure the property is available in property file.");
        }
    }
}
