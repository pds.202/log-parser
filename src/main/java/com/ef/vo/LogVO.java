package com.ef.vo;

import java.util.Optional;

public class LogVO {
    String eventTime;
    String ipAddress;
    String request;
    Integer httpStatus;
    String userAgent;

    public LogVO() {
    }

    public LogVO(String eventTime, String ipAddress, String request, Integer httpStatus, String userAgent) {
        this.eventTime = eventTime;
        this.ipAddress = ipAddress;
        this.request = request;
        this.httpStatus = httpStatus;
        this.userAgent = userAgent;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String header() {
        return "eventTime" + ", ipAddress" + ", request" + ", httpStatus"  + ", userAgent";
    }

    @Override
    public String toString() {
        return  eventTime + ", " + ipAddress  + ", " + request + ", " + httpStatus + ", " + userAgent;
    }

    static final String dateTimeFormat = "yyyy-MM-dd HH:mm:ss.S"; //2017-01-01 00:02:36.179
    public Optional<LogVO> lineToLogVo(String line, String delimiter){
        String [] splitted = line.split(delimiter);

        if(splitted.length == 5){
            //LocalDateTime givenTime = Util.formatDateTime(splitted[0], dateTimeFormat);
            String givenTime = splitted[0];
            String ip = splitted[1];
            String request = splitted[2];
            Integer httpStatus = Integer.parseInt(splitted[3]);
            String ua = splitted[4];

            LogVO returnObj = new LogVO(givenTime,ip, request,httpStatus,ua);
            return Optional.of(returnObj);

        }else {
            return Optional.empty();
        }

    }

}
