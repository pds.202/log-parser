package com.ef.vo;

import com.ef.constants.DurationEnum;

import java.time.LocalDateTime;

public class CommandArgs {
    private String accessLog;
    private LocalDateTime startDate;
    private DurationEnum duration;
    private Integer threshold;

    public CommandArgs() {
    }

    public CommandArgs(String accessLog, LocalDateTime startDate, DurationEnum duration, Integer threshold) {
        this.accessLog = accessLog;
        this.startDate = startDate;
        this.duration = duration;
        this.threshold = threshold;
    }

    public String getAccessLog() {
        return accessLog;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public DurationEnum getDuration() {
        return duration;
    }

    public Integer getThreshold() {
        return threshold;
    }

    @Override
    public String toString() {
        return "CommandArgs{" +
                "accessLog='" + accessLog + '\'' +
                ", startDate=" + startDate +
                ", duration=" + duration +
                ", threshold=" + threshold +
                '}';
    }

}
