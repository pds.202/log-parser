package com.ef.db;

import com.ef.common.Util;
import com.ef.constants.DurationEnum;
import com.ef.constants.GlobalProperties;
import com.ef.vo.LogVO;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBService {

    public void postLogToDatabase(String logFile) throws Exception {

        try (Connection connection = ConnectionUtil.getConnection()) {
            checkIfFileIsValid(logFile);

            createEmptyLogTable(connection);
            createEmptyBlockedHostsTable(connection);
            System.out.println(LocalDateTime.now() + " table created...");

            List<String> badRecords = new ArrayList<>();
            List<LogVO> recordsToInsert = new ArrayList<>();
            System.out.println("starting ...");

            Files.lines(Paths.get(logFile)).forEach(line -> {
                LogVO logRecord = (new LogVO()).lineToLogVo(line, GlobalProperties.delimiter).orElse(null);
                if (logRecord != null) {
                    recordsToInsert.add(logRecord);
                } else {
                    badRecords.add(line);
                }
            });

            System.out.println("inserting data to table");
            insertIntoTable(recordsToInsert, connection);
            System.out.println("logRecord size: " + recordsToInsert.size());
            System.out.println("bad records size:" + badRecords.size());
            System.out.println(LocalDateTime.now() + " done");

        }
    }

    private boolean checkIfFileIsValid(String logFile) throws IOException {
        File f = new File(logFile);
        if (f.isFile() && f.canRead()) {
            return true;
        } else {
            throw new IOException("Error reading " + logFile + "\nplease check if the file exists and has read permission");
        }
    }

    private void createEmptyLogTable(Connection conn) throws Exception {
        try (Statement statement = conn.createStatement()) {
            boolean newTable = statement.execute(GlobalProperties.createAccessLogTableQuery);
            if (!newTable) {
                System.out.println("truncating accessLog");
                statement.execute(GlobalProperties.truncateAccessLogTableQuery);
            }
        }
    }

    private void createEmptyBlockedHostsTable(Connection conn) throws Exception {
        try (Statement statement = conn.createStatement()) {
            boolean newTable = statement.execute(GlobalProperties.createBlokedHostsTableQuery);
            if (!newTable) {
                System.out.println("truncating blocked hosts");
                statement.execute(GlobalProperties.truncateBlokedHostsTableQuery);
            }
        }
    }

    private void insertIntoTable(List<LogVO> recordsToInsert, Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(GlobalProperties.insertLogQuery);) {
            connection.setAutoCommit(false);
            int count = 0;
            System.out.println("count = " + count);
            for (LogVO entity : recordsToInsert) {
                preparedStatement.setString(1, entity.getEventTime());
                preparedStatement.setString(2, entity.getIpAddress());
                preparedStatement.setString(3, entity.getRequest());
                preparedStatement.setInt(4, entity.getHttpStatus());
                preparedStatement.setString(5, entity.getUserAgent());

                preparedStatement.addBatch();
                count += 1;
                if (count % 100_000 == 0) {
                    preparedStatement.executeBatch();
                    connection.commit();
                    preparedStatement.clearBatch();
                }
            }
            preparedStatement.executeBatch();
            connection.commit();
            preparedStatement.clearBatch();
        } catch (SQLException e) {
            System.out.println("error:");
            e.printStackTrace();
        }
    }

    public void blockHosts(Map<String, Integer> filteredLogRecords, LocalDateTime startDateTime, DurationEnum duration, int threshold) throws Exception {
        LocalDateTime endDateTime = Util.getEndDateTime(startDateTime, duration);

        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GlobalProperties.insertBlockedHostsQuery);) {

            //connection.setAutoCommit(false);
            for (String ip : filteredLogRecords.keySet()) {
                Integer numberOfCalls = filteredLogRecords.get(ip);
                String reason = Util.buildReason(startDateTime, endDateTime, threshold, numberOfCalls);

                preparedStatement.setString(1, ip);
                preparedStatement.setInt(2, numberOfCalls);
                preparedStatement.setString(3, reason);

                preparedStatement.execute();
                //System.out.printf("inserted:: %s %d\n",ip, numberOfCalls);
                //connection.commit();
            }
        }
    }

    public Map<String, Integer> filterLog(LocalDateTime startDateTime, DurationEnum duration, int threshold) throws Exception {
        LocalDateTime endDateTime = Util.getEndDateTime(startDateTime, duration);
        return executeFilterQuery(startDateTime, endDateTime, threshold);
    }

    private Map<String, Integer> executeFilterQuery(LocalDateTime startDateTime, LocalDateTime endDateTime, int threshold) throws Exception {
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GlobalProperties.filterLogQuery);) {

            preparedStatement.setString(1, startDateTime.toString());
            preparedStatement.setString(2, endDateTime.toString());
            preparedStatement.setInt(3, threshold);

            Map<String, Integer> result = new HashMap<>();

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.put(resultSet.getString(1), resultSet.getInt(2));
                }
            }

            return result;
        } catch (Exception e) {
            System.err.println("Error while calling database");
            throw e;
        }
    }


}
