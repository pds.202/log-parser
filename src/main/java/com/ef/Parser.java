package com.ef;

import com.ef.common.Util;
import com.ef.db.DBService;
import com.ef.vo.CommandArgs;
import org.apache.commons.cli.Options;

import java.util.Map;
import java.util.Objects;

public class Parser {

    public static void main(String[] params) {
        Options options = Util.parseCliArgs();
        CommandArgs args = Util.buildArgs(options, params);

        DBService dbService = new DBService();
        postLogToDatabase(dbService, args.getAccessLog());

        if(Objects.nonNull(args.getStartDate()) && Objects.nonNull(args.getThreshold())) {
            Map<String, Integer> filteredLogRecords = filterLog(dbService, args);
            blockHosts(dbService, filteredLogRecords, args);
            System.out.println("ip,             count");
            filteredLogRecords.keySet().forEach(key -> System.out.println(key + ", " + filteredLogRecords.get(key)));
        }
    }

    private static void blockHosts(DBService dbService, Map<String,Integer> filteredLogRecords, CommandArgs args) {
        try {
            dbService.blockHosts(filteredLogRecords, args.getStartDate(), args.getDuration(),args.getThreshold());
        } catch (Exception e) {
            System.out.println("blockHosts:: " + e.getMessage());
            System.exit(1);
        }
    }

    private static Map<String,Integer> filterLog(DBService dbService, CommandArgs args) {
        try {
            return dbService.filterLog(args.getStartDate(), args.getDuration(),args.getThreshold());
        } catch (Exception e) {
            System.out.println("filterLog:: " + e.getMessage());
            System.exit(1);
            return null;
        }
    }

    private static void postLogToDatabase(DBService dbService, String accessLog) {
        if (accessLog != null ){
            try {
                dbService.postLogToDatabase(accessLog);
            } catch (Exception e){
                System.out.println("postLogToDatabase:: " + e.getMessage());
                System.exit(1);
            }
        }
    }
}