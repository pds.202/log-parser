﻿The goal is to write a parser in Java that parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. 

Java
----

(1) Create a java tool that can parse and load the given log file to MySQL. The delimiter of the log file is pipe (|)

(2) The tool takes "startDate", "duration" and "threshold" as command line arguments. "startDate" is of "yyyy-MM-dd.HH:mm:ss" format, "duration" can take only "hourly", "daily" as inputs and "threshold" can be an integer.

(3) This is how the tool works:

    java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
	
	The tool will find any IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00 (one hour) 
	and print them to console AND also load them to another MySQL table with comments on why it's blocked.

    [OUTPUT}:
        ip,             count
        192.168.77.101, 214
        192.168.228.188, 209

	java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250

	The tool will find any IPs that made more than 250 requests starting from 2017-01-01.13:00:00 to 2017-01-02.13:00:00 (24 hours) 
	and print them to console AND also load them to another MySQL table with comments on why it's blocked.

    [OUTPUT}:
        ip,             count
        192.168.203.111, 300
        192.168.162.248, 281
        192.168.38.77, 336
        192.168.51.205, 296
        192.168.129.191, 350
        192.168.62.176, 279
        192.168.199.209, 308
        192.168.219.10, 254
        192.168.143.177, 332
        192.168.31.26, 263
        192.168.52.153, 273
        192.168.33.16, 265

SQL
---

(1) Write MySQL query to find IPs that mode more than a certain number of requests for a given time period.

    Ex: Write SQL to find IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00.

	[QUERY]:
	    select ip,count(1) as cnt from wh.access_log
		    where event_time between '2017-01-01.13:00:00' and '2017-01-01.14:00:00'
		    group by ip having cnt > 100 order by cnt desc;

(2) Write MySQL query to find requests made by a given IP.

	[QUERY]:
	    select log.request from wh.access_log log where log.ip = '192.168.77.101';

LOG Format
----------
Date, IP, Request, Status, User Agent (pipe delimited, open the example file in text editor)

Date Format: "yyyy-MM-dd HH:mm:ss.SSS"

Also, please find attached a log file for your reference. 

The log file assumes 200 as hourly limit and 500 as daily limit, meaning:

(1) 
When you run your parser against this file with the following parameters

java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.15:00:00 --duration=hourly --threshold=200

The output will have 192.168.11.231. If you open the log file, 192.168.11.231 has 200 or more requests between 2017-01-01.15:00:00 and 2017-01-01.15:59:59

(2) 
When you run your parser against this file with the following parameters

java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500

The output will have  192.168.102.136. If you open the log file, 192.168.102.136 has 500 or more requests between 2017-01-01.00:00:00 and 2017-01-01.23:59:59


Deliverables
------------

(1) Java program that can be run from command line
	
    java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
        [attached]

(2) Source Code for the Java program
        https://gitlab.com/pds.202/log-parser

(3) MySQL schema used for the log data
    [Log data table]:
        create table access_log(
            id int not null AUTO_INCREMENT,
            event_time timestamp,
            ip  varchar(20),
            request varchar(100),
            http_status int,
            user_agent varchar(1000),
            PRIMARY KEY (id));

    [Blocked hosts table]:
        create table blocked_hosts(
            id int not null AUTO_INCREMENT,
            ip  varchar(20),
            request_count int,
            reason varchar(1000),
            PRIMARY KEY (id));

(4) SQL queries for SQL test
    [Blocked Hosts]:
        select * from blocked_hosts;

    [Count by ipaddress for given time block]:
        select ip,count(1) as cnt from wh.access_log
        		where event_time between '2017-01-01.13:00:00' and '2017-01-01.14:00:00'
        		group by ip having cnt > 100 order by cnt desc;

